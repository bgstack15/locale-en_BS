Format: 3.0 (quilt)
Source: locale-en-bs
Binary: locale-en-bs
Architecture: all
Version: 0.0.1-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.ddns.net/
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12)
Package-List:
 locale-en-bs deb localization optional arch=all
Files:
 00000000000000000000000000000000 1 locale-en-bs.orig.tar.gz
 00000000000000000000000000000000 1 locale-en-bs.debian.tar.xz
