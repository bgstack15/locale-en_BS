# Startdate: 2022-03-05 09:55
# Reference:
#    7w.spec
#    local-en_RU.spec
%global  _locale en_BS
%global  use_master 1
%global _localesdir %{_datadir}/i18n/locales
%global _localedef %{_bindir}/localedef

%global vername master
%if 0%{?use_master}
%global vername %{version}
%endif

Name:    locale-%{_locale}
Version: 0.0.1
Release: 2
Summary: English locale for Stackrpms
License: GPLv3+
URL:     https://bgstack15.ddns.net/
Source0: %{_locale}
Packager:   B. Stack <bgstack15@gmail.com>
Requires(post): %{_localedef}
Requires(post): %{_localesdir}
%if 0%{?el8}%{?fedora}
Requires: glibc-locale-source
Requires(post): glibc-locale-source
Requires(preun): glibc-locale-source
%endif
Requires(preun): %{_localedef}
Requires(preun): %{_localesdir}
BuildArch:  noarch

%description
This en_BS locale fixes the en_US timestampes to make
more sense. This is intended for internal use at
Stackrpms locations.

%install
%{__install} -d %{buildroot}%{_localesdir} -m0755
%{__install} -t %{buildroot}%{_localesdir} -m0644 %{SOURCE0}

%post
%{_localedef} --quiet --force --inputfile %{_localesdir}/%{_locale} --charmap UTF-8 %{_locale}.utf8 || :

%preun
if test "${1}" = "0" ;
then
   %{_localedef} --quiet --delete-from-archive %{_locale}.utf8
fi

%files
%defattr(-,root,root,0)
%{_localesdir}/%{_locale}

%changelog
* Mon Mar 28 2022 B. Stack <bgstack15@gmail.com> - 0.0.1-2
- Add dep glibc-locale-source

* Sat Mar 05 2022 B. Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial package
