# Readme for `locale-en_BS`

## `locale-en_BS` upstream
This is an original [package](https://gitlab.com/bgstack15/locale-en_BS). It contains merely the customized locale for GNU C Library that I prefer.

## Reason for existing
To practice with locales, as well as make the default date stamps more sane (I'm looking at you, Thunar!).

## Alternatives
Use `en_US` like the majority of the systems in this great nation.

## Dependencies
Glibc. The internationalization of other C libraries is undetermined, but also not important for my use case.

Package recipes are available for [rpm](locale-en_BS.spec) and [dpkg](debian/rules).

## Installing

### Rpm
Visit the [copr package](https://copr.fedorainfracloud.org/coprs/bgstack15/stackrpms/package/locale-en_BS/).

### Dpkg
Visit the [obs package](https://build.opensuse.org/package/show/home:bgstack15/locale-en-bs).

### Manual
The `en_BS` file is the bare locale file. You can use it in your own GNU environment by manually compiling it and setting your system to use it.

Compile the file to the default location (requires root).

    sudo localedef -i en_BS -f UTF-8 en_BS.UTF-8

Now the locale is available to use, until the next time glibc is updated. Use the packages for persistence. To use the locale, you can do this on Devuan:

    sudo update-locale LC_TIME=en_BS

Or this on Fedora:

    sudo localectl set-locale LC_TIME=en_BS

Or alternatively:

    echo 'export LC_TME=en_BS.utf8' | sudo tee -a /etc/environment

## References

### Weblinks
1. `en_RU` project: [Readme](https://en-ru.sourceforge.io/en.html) and main [Sourceforge page](https://sourceforge.net/projects/en-ru/)
2. [customization - How can I customize a system locale? - Ask Ubuntu](https://askubuntu.com/questions/21316/how-can-i-customize-a-system-locale/162714#162714)
3. [Set custom locales in Gnome3 (on Fedora 20) - Unix &amp; Linux Stack Exchange](https://unix.stackexchange.com/questions/136920/set-custom-locales-in-gnome3-on-fedora-20)
4. [command line - How can I change the default date format (using LC_TIME)? - Ask Ubuntu](https://askubuntu.com/questions/1064167/how-can-i-change-the-default-date-format-using-lc-time)

### Man pages
`localedef(1)`
`update-locale(8)`
